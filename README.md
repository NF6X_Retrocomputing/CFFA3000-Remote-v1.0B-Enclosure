# CFFA3000 Remote v1.0B Enclosure

## Description

This is an enclosure for the CFFA3000 board's remote control. The
CFFA3000 is a modern floppy disk and hard disk emulator for the Apple II
series computers. Its remote control is a bare board, so I designed
this enclosure for it. My remote control board is marked "v1.0B", and
I don't know if this enclosure is compatible with any other versions.

This enclosure was designed with Autodesk Fusion 360. I have included the
.f3d source file for use with Fusion 360, .stp files for use with most
3D solid modeling software, .stl files for import into slicing
utilities, and the .gcode file that I created with Cura to print this
on my MP Select Mini 3D Printer.

## Parts Required

In addition to this enclosure, you will also need four each #6-32 x 3/8"
self-tapping flathead screws, such as McMaster-Carr part number 90086A146.

## Pictures

![](pics/IMG_3763.jpg?raw=true)

![](pics/IMG_3766.jpg?raw=true)

![](pics/IMG_3767.jpg?raw=true)

![](pics/IMG_3768.jpg?raw=true)

![](pics/IMG_3769.jpg?raw=true)

## License

This design is open source and public domain. Use at your own risk of
delight.

